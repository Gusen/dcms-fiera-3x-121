<?

#---------------------установка языка-----------------------------#
//если гость
if (empty($user))
    $set['lang'] = isset($_SESSION['lang']) ? $_SESSION['lang'] : $set['lang'];
//проверка на левые символы
if (!preg_match('#^[A-z0-9-._]$#ui', $set['lang']))
    // preg_match необязательно, если делать проверку достаточно in_array'
    //если норм
    $set['lang'] = $set['lang'];
else //если что-то не так
    $set['lang'] = 'ru';
//если авторизован
if (isset($user))
    $set['lang'] = $user['lang'];
//если пусто
if ($set['lang'] == null)
    $set['lang'] = 'ru';
if (isset($_SESSION['lang']) && $_SESSION['lang'] == null)
    $_SESSION['lang'] = $set['lang'];
#---------------------установка языка-----------------------------#


